from calculator import Calculator

while True:
    print("#### CALCULATOR ####")
    num1 = int(input("Enter first number: "))
    num2 = int(input("Enter second number: "))
 
    print("Select operation.")
    print("1. Add")
    print("2. Subtract")
    print("3. Multiply")
    print("4. Divide")
    print("5. Power")
    print("6. Root")
    print ("7. Percentage")
 
    choice = input("Enter choice(1/2/3/4/5/6/7):")
    result = 0

    if choice == '1':
        result = Calculator.add(num1, num2)
        print("The result of", num1, "+",num2,"is", result)

    elif choice == '2':
        result = Calculator.subtract(num1, num2)
        print("The result of", num1, "-",num2,"is", result)

    elif choice == '3':
        result = Calculator.multiply(num1, num2)
        print("The result of", num1, "x",num2,"is", result)

    elif choice == '4':
        if num2 == 0:
            print("No puedo dividir por 0, selecciona otro número.")
        else:
            result = Calculator.divide(num1, num2)
            print("The result of", num1, "÷",num2,"is", result)

    elif choice == '5':
        result = Calculator.power(num1, num2)
        print("The result of", num1, "^",num2,"is", result)

    elif choice == '6':
        if num1%2 == 0 and num2<0:
            print ("No puedo hacer raiz par de un numero negativo.")
        else:
            result = Calculator.root(num1, num)
            print("The result of", num1, "√",num2,"is", result)

    elif choice == '7':
        result = Calculator.percentage(num1, num2)
        print("The", num1, "% of",num2,"is", result)
        
    else:
        print("Invalid Input")
 
 
    quit = input("Do you want to continue (Y/n)?")
    if quit == 'n':
        break
    elif quit == '' or quit =='y' or quit =='Y':
        continue
    else:
        break
