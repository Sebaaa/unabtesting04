class Calculator:

    def add(x, y):
        return x + y

    def subtract(x, y):
        return x - abs(y)
     
    def multiply(x, y):
        return x * y
     
    def divide(x, y):
        if y == 0:
            return
        else:
            return x / y
    
    def power(x,y):
        return x ** y

    def root(x,y):
        if x%2 == 0 and y<0:
            return
        else:
            return y**(1/x)

    def percentage (x,y):
        return (x*y)/100