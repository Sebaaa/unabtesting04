Esta es la aplicación de la calculadora en python que se evaluará Testing. 

Procedí a añadir nuevas funcionalidades: Potencia, Raiz y Porcentaje.

Existen creadas un total de 20 pruebas unitarias.

[Aquí](https://drive.google.com/open?id=19rgJauepqnUcYGIAXgHryuH7QeWxK0IO) se muestra que todas las pruebas unitarias pasaron.

Y [aquí](https://drive.google.com/open?id=1smLqGhDit0KCMUJzltP-xgGeR3-Xq55l) se muestra la cobertura total del código.


