import unittest
from app.calculator import Calculator


class TestCalculatorAdd(unittest.TestCase):

        def test_add_positive(self):
            result = Calculator.add(1,100)
            self.assertEqual(result, 101)
        def test_add_negative(self):
            result = Calculator.add(-1,-100)
            self.assertEqual(result, -101)
        def test_add_positive_negative(self):
            result = Calculator.add(15,-10)
            self.assertEqual(result, 5)

class TestCalculatorSubstract(unittest.TestCase):

        def test_substract_positive(self):
            result = Calculator.subtract(5,400)
            self.assertEqual(result, -395)
        def test_substract_negative(self):
            result = Calculator.subtract(-100,-300)
            self.assertEqual(result, -400)
        def test_subtract_negative_positive(self):
            result = Calculator.subtract(-15,10)
            self.assertEqual(result, -25)

class TestCalculatorMultiply(unittest.TestCase):

        def test_multiply_negative(self):
            result = Calculator.multiply(-3,-5)
            self.assertEqual(result, 15)
        def test_multiply_positive(self):
            result = Calculator.multiply(10,10)
            self.assertEqual(result, 100)
        def test_multiply_positive_negative(self):
            result = Calculator.multiply(15,-10)
            self.assertEqual(result, -150)

class TestCalculatorDivide(unittest.TestCase):
        def test_divide_positive(self):
            result = Calculator.divide(4,2)
            self.assertEqual(result, 2)
        def test_divide_byzero(self):
            result = Calculator.divide(4,0)
            self.assertIsNone(result)

class TestCalculatorPower(unittest.TestCase):


        def test_power_positive(self):
            result = Calculator.power(2,6)
            self.assertEqual(result, 64)
        def test_power_negative(self):
            result = Calculator.power(-2,-3)
            self.assertEqual(result, -0.125)
        def test_power_positive_negative(self):
            result = Calculator.power(5,-2)
            self.assertEqual(result, 0.04)

class TestCalculatorRoot(unittest.TestCase):

        def test_root_positive(self):
            result = Calculator.root(2,100)
            self.assertEqual(result, 10.0)
        def test_root_of_zero(self):
            result = Calculator.root(6,0)
            self.assertEqual(result, 0)
        def test_root_of_negative(self):
            result = Calculator.root(2,-49)
            self.assertIsNone(result)
            
class TestCalculatorPercentage(unittest.TestCase):


        def test_percentage_positive(self):
            result = Calculator.percentage(10,1000)
            self.assertEqual(result, 100)
        def test_percentage_negative(self):
            result = Calculator.percentage(-20,-250)
            self.assertEqual(result, 50)
        def test_percentage_positive_negative(self):
            result = Calculator.percentage (15,-300)
            self.assertEqual(result, -45)


if __name__ == '__main__':
    unittest.main()
